# UFSC 3D Composite Viewer
A 3D composite viewer created for the purpose of the Unfavourable Semicircle effort.

* https://reddit.com/r/UnfavorableSemicircle
* https://www.unfavorablesemicircle.com/wiki/UnfavorableSemicircle_Wiki
* https://discord.gg/0153LdFL1F3RQmns9


### Instructions:
 - WSAD + Space + Ctrl + Mouse: Movement,
 - Esc: Exit,
 - R: Reset camera,
 - M: Change viewing mode, RGB <=> HSV,
 - P: Change perspective, Perspective <=> Orthogonal
 - F: Toggle fullscreen,
 - B: Switch between black and white background,
 - F2: Take screenshot.

Put the image (2D composite) in the program directory and name it "input.png" before running.
**Maxumum supported size is 16384x1634 due to gpu limitations.**