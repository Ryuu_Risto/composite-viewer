extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var composite = $composite;
var generated = true
var taken
var colors
var bcg = false
var mode = false
var fs = false
var thread
var lock = false

# Called when the node enters the scene tree for the first time.
func _ready():
	OS.set_window_size(Vector2(1600, 900))
	var pos = (OS.get_screen_size() - Vector2(1600, 900)) /2
	OS.set_window_position(pos)
	update_text()
	if ResourceLoader.exists("./input.png"):
		thread = Thread.new()
		thread.start(self, "_thread_render")
	else:
		print("No input file found.")
		generated = false

func _thread_render(data):
	lock = true
	print("Importing image")
	generate_taken()
	var image = Image.new()
	image.load("./input.png")
	image.lock()
	print("Image loaded, pulling data")
	var size = image.get_size();
	#first pass
	colors = Array()
	for x in range(size.x):
		for y in range(size.y):
			var color = image.get_pixel(x, y)
			if taken[color.r8][color.g8][color.b8] == false:
				taken[color.r8][color.g8][color.b8] = true
				colors.append(color)
	print("Colors counted (", colors.size(), "), visualising")
	update_text()
	composite.multimesh.instance_count = colors.size()
	#second pass
	call_deferred("_thread_grgb", colors)

func _exit_tree():
	if generated:
		thread.wait_to_finish()

func _thread_grgb(arr):
	lock = true
	for c in range(arr.size()):
		var position = Transform()
		position = position.translated(Vector3(arr[c].r, arr[c].g, arr[c].b))
		composite.multimesh.set_instance_transform(c, position)
		composite.multimesh.set_instance_color(c, Color(arr[c].r, arr[c].g, arr[c].b, 1.0))
	print("Generation finished (RGB)")
	lock = false

func _thread_ghsv(arr):
	lock = true
	for c in range(arr.size()):
		var position = Transform()
		position = position.translated(Vector3(arr[c].h, arr[c].s, arr[c].v))
		composite.multimesh.set_instance_transform(c, position)
	print("Generation finished (HSV)")
	lock = false

func generate_taken():
	taken = Array()
	for x in range(256):
		taken.append([])
		for y in range(256):
			taken[x].append([])
			for z in range(256):
				taken[x][y].append(false)

func update_text():
	$camera/gui/resolution.set_text("Resolution: " + String(OS.get_window_size().x) + "x" + String(OS.get_window_size().y))
	if not mode:
		$camera/gui/mode.set_text("Mode: RGB")
	else:
		$camera/gui/mode.set_text("Mode: HSV")
	if typeof(colors) == TYPE_NIL:
		$camera/gui/count.set_text("Colour count: Awaiting count")
	else:
		$camera/gui/count.set_text("Colour count: " + String(colors.size()))
	if not $camera.proj:
		$camera/gui/projection.set_text("Projection: Perspective")
	else:
		$camera/gui/projection.set_text("Projection: Ortho")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (Input.is_action_just_pressed("change_background")):
		if not bcg:
			$WorldEnvironment.get_environment().set_bg_color(Color(1.0, 1.0, 1.0, 1.0))
			bcg = true
		else:
			$WorldEnvironment.get_environment().set_bg_color(Color(0.0, 0.0, 0.0, 1.0))
			bcg = false
	if (Input.is_action_just_pressed("fullscreen")):
		if not fs:
			OS.set_borderless_window(true)
			OS.set_window_size(OS.get_screen_size())
			OS.set_window_position(Vector2(0, 0))
			fs = true
		else:
			OS.set_borderless_window(false)
			OS.set_window_size(Vector2(1600, 900))
			var pos = (OS.get_screen_size() - Vector2(1600, 900)) /2
			OS.set_window_position(pos)
			fs = false
		update_text()
	if (Input.is_action_just_pressed("change_mode")):
		if not lock:
			if mode:
				thread.wait_to_finish()
				thread.start(self, "_thread_grgb", colors)
				mode = false
			else:
				thread.wait_to_finish()
				thread.start(self, "_thread_ghsv", colors)
				mode = true
			update_text()
	if (Input.is_action_just_pressed("screenshot")):
		# start screen capture
		get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
		# Let two frames pass to make sure the screen was captured
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
	
		# Retrieve the captured image
		var img = get_viewport().get_texture().get_data()
	  
		# Flip it on the y-axis (because it's flipped)
		img.flip_y()
		# save to a file
		img.save_png("capture.png")
		print("Screenshot saved")
