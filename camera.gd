extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var camera_angle = 0.0
var sensitivity = 0.1
var direction = Vector3()
const speed = 10
var proj = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (Input.is_action_just_pressed("exit")):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().quit()
	if (Input.is_action_just_pressed("perspective_mode")):
		if not proj:
			$head/Camera.projection = Camera.PROJECTION_ORTHOGONAL
			proj = true
		else:
			$head/Camera.projection = Camera.PROJECTION_PERSPECTIVE
			proj = false
		get_node("..").update_text()
	if (Input.is_action_just_pressed("reset_cam")):
		self.set_translation(Vector3(-12.7, 12.7, 12.7))
		$head.set_rotation_degrees(Vector3(0.0, 0.0, 0.0))
		$head/Camera.set_rotation_degrees(Vector3(0.0, 0.0, 0.0))
		self.set_rotation_degrees(Vector3(0.0, -90.0, 0.0))
		camera_angle = 0.0

func _physics_process(delta):
	direction = Vector3()
	var aim = $head/Camera.get_camera_transform().basis
	
	if Input.is_action_pressed("forward"):
		direction -= aim.z
	if Input.is_action_pressed("backward"):
		direction += aim.z
	if Input.is_action_pressed("left"):
		direction -= aim.x
	if Input.is_action_pressed("right"):
		direction += aim.x
	if Input.is_action_pressed("down"):
		direction -= aim.y
	if Input.is_action_pressed("up"):
		direction += aim.y
	direction = direction.normalized()
	move_and_slide(direction * speed)

func _input(event):
	if event is InputEventMouseMotion:
		$head.rotate_y(-deg2rad(event.relative.x * sensitivity))
		var change = -event.relative.y * sensitivity
		if change + camera_angle < 90 and change + camera_angle > -90:
			$head/Camera.rotate_x(deg2rad(change))
			camera_angle += change
